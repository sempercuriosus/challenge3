<!-- Title  -->

# Challenge 3 - Javascript Password Generator

This app will generate a password with a specified length and type of characters

## Table of Contents

<!-- Table of Contents -->

- [About The Project](#about_project)
- [Getting Started](#getting_started)
- [Deployment Location](#deployment_location)
- [Challenges](#challenges)
- [Final Note](#final_note)
- [Author Credit](#author_credit)
- [Acknowledgments](#acknowledgments)

---

## About The Project <a id="about_project"></a>

<!-- About the Project -->

This is a class project, with provided HTMLS and CSS. I was asked to create a password generator which will make a password meeting the user's criteria of length, uppercase, lowercase, special characters, and digits. One can select any of the following items to include or ignore.

---

### Built With

<!-- Built With -->

This is primarily built with Javascript! There are some HTML and CSS components as well, but those I did not write.

### Features

<!-- Features -->

The app will return a password of a specific length and character-sets, displaying the password in the main window.

## Getting Started <a id="getting_started"></a>

<!-- Getting Started  -->

### Prerequisites & Dependencies

<!-- Prerequisites & Dependencies-->

- There are no prerequisites & dependencies

### Installation

<!-- Installation -->

- This runs on the web page, so there is not installation needed.

### Usage

<!-- Usage -->

#### Configurables

You will be prompted for the options allowed for the password. That will include:

- Length
- Uppercase Characters
- Lowercase Characters
- Special Characters
- Numbers

<!-- Configurables -->

### Running The App

<!-- Running -->

- When ready to run the app, click the red "Generate Password" button, and the above prompts will ask for what options you want to use. When the app is complete, in the middle of the web page, the password will be displayed.

- When each prompt pops up:
  - Click "Cancel" or hit the "ESC" key to not include an option
  - Click "OK" or hit the "Enter" key to confirm an option

---

## Deployment Location <a id="deployment_location"></a>

<!-- Deployment Location -->

You can find the live version of the application here [Password Generator](https://sempercuriosus.github.io/challenge3/)

Here is a brief image of how the application looks.
![Deploy Image Sample](./assets/image.png)

---

## Challenges <a id="challenges"></a>

<!-- Challenges -->

- When trying to Push a new value to an array I was getting a .push is not a function.
  - How that was resolved: initially, I was trying to assign `passwordArray.push(randomChar);` to a variable. When I removed the assignement statement this solved the issue.
- Understaning how to set the object properties:
  - When returning invalid items from a prompt, and checking the resulting values, returning null in the function checking conditions, will not set the value to the object.
- Trying to balance the single responsibilty nature of the functions and not overdoing breaking things up to be too small in nature. IE making the code harder to read than needed.

---

## Final Note <a id="final_note"></a>

<!-- Final Note -->

- This was an interesting challenge to work with javascript starting to show some of what can be done when adding JS to HTML and CSS.

---

## Author Credit <a id="author_credit"></a>

<!-- Author Credits -->

- Eric (sempercuriosus)

---

## Acknowledgments <a id="acknowledgments"></a>

<!-- Acknowledgments -->

- I used the starter code's character arrays to avoid typing them out and missing one.
- I also used the Xpert AI provided to help me debug when I got stuck on some of the operations.

## Resources

<!-- Resources -->

- https://www.w3schools.com/Jsref/jsref_obj_array.asp
  - Used to find array methods.
- https://javascript.info/alert-prompt-confirm
  - Finding actions to use.

---
