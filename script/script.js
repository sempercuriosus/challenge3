// Assignment Code
var generateBtn = document.querySelector("#generate");

// Write password to the #password input
function writePassword() {
  var password = generatePassword();
  var passwordText = document.querySelector("#password");

  passwordText.value = password;
}

let passwordUserOptions = {
  length: 0,
  useLowerCase: false,
  useUpperCase: false,
  useSpecialChar: false,
  useNumericChar: false,
};

let passwordCharsIncluded = [];

// list of possible chars to select from
const lowerCaseCharList = [
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
];

const upperCaseCharList = [
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
];

const specialCharList = [
  "@",
  "%",
  "+",
  "\\",
  "/",
  "'",
  "!",
  "#",
  "$",
  "^",
  "?",
  ":",
  ",",
  ")",
  "(",
  "}",
  "{",
  "]",
  "[",
  "~",
  "-",
  "_",
  ".",
];

const numericCharList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

function generatePassword() {
  let password = "";

  // asking the user for their input requirements
  askLength();
  askLowerCase();
  askUpperCase();
  askNumeric();
  askSpecialChars();

  // use the password options and then build the password
  getPasswordOptions();
  password = buildPassword();

  return password;
}
// end [ generatePassword ]

// ask user for
/*
  length

  lower case
  upper case
  numeric
  special characters

*/

function askLength() {
  let passwordLength = 0;
  passwordLength = parseInt(
    prompt("How long should the password be?", "Password Length"),
    10
  );

  if (isNaN(passwordLength)) {
    alert("You must input a number for the length.");
    return null;
  }

  if (passwordLength < 8) {
    alert("Your password must be longer than 8 characters.");
    return null;
  }

  if (passwordLength > 128) {
    alert("Your password must not exceed 128 characters.");
    return null;
  }

  passwordUserOptions["length"] = passwordLength;
}
// end [ askLength ]

function askUpperCase() {
  passwordUserOptions["useUpperCase"] = confirm(
    "Include Uppercase Characters?",
    "Uppecase"
  );
}
// end [ askUpperCase ]

function askLowerCase() {
  passwordUserOptions["useLowerCase"] = confirm(
    "Include Lowercase Characters?",
    "Lowercase"
  );
}
// end [ askLowerCase ]

function askNumeric() {
  passwordUserOptions["useNumericChar"] = confirm(
    "Include Numeric Characters?",
    "Numbers"
  );
}
// end [ askNumeric ]

function askSpecialChars() {
  passwordUserOptions["useSpecialChar"] = confirm(
    "Include Special Characters?",
    "Special Characters"
  );
}
// end [ askSpecialChars ]

function getPasswordOptions() {
  // reset the array so nothing persists and adds chars not wanted.
  passwordCharsIncluded = [];

  if (passwordUserOptions["useUpperCase"] === true) {
    passwordCharsIncluded = passwordCharsIncluded.concat(upperCaseCharList);
  }

  if (passwordUserOptions["useLowerCase"] === true) {
    passwordCharsIncluded = passwordCharsIncluded.concat(lowerCaseCharList);
  }

  if (passwordUserOptions["useNumericChar"] === true) {
    passwordCharsIncluded = passwordCharsIncluded.concat(numericCharList);
  }

  if (passwordUserOptions["useSpecialChar"] === true) {
    passwordCharsIncluded = passwordCharsIncluded.concat(specialCharList);
  }
}
// end [ getPasswordOptions ]

function buildPassword() {
  let passwordArray = [];
  let password = "";
  let upperLimit = passwordUserOptions["length"];

  if (passwordCharsIncluded.length == 0) {
    alert("Please select at least one of the character options to use.");
  } else {
    // take the new char array and get a random char for the length desired
    for (let i = 0; i < upperLimit; i++) {
      let randomNumber = getRandomNumber();
      let randomChar = getRandomChar(randomNumber);

      passwordArray.push(randomChar);
    }

    // join the final array into a string
    password = passwordArray.join("");
  }

  return password;
}
// end [ buildPassword ]

function getRandomNumber() {
  let randomNumber = 0;

  // get the random number to use as the index to get a value from the joined array.
  randomNumber = Math.floor(Math.random() * passwordCharsIncluded.length);

  return randomNumber;
}
// end [ getRandomNumber ]

function getRandomChar(indexValue) {
  let randomChar = "";
  randomChar = passwordCharsIncluded[indexValue];

  return randomChar;
}
// end [ getRandomChar ]

// Add event listener to generate button
generateBtn.addEventListener("click", writePassword);
